<?php

namespace App\Http\Controllers;

use App\Secret;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class SecretController extends Controller
{
  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'message' => 'required',
      'password' => 'required|min:6',
    ]);

    $secret = new Secret;
    $secret->message = $this->personalEncrypt($request->message, $request->password);
    $secret->password = Hash::make($request->password);
    // dd($secret->message, utf8_encode($secret->message), utf8_decode(utf8_encode($secret->message)));
    
    $secret->save();

    $request->session()->now('status', 'Secret is stored and available by link ');
    $request->session()->now('newsecreturl', url('secret/' . $secret->id));
    return view('newsecret');
  }

  public function retrieveById($id, Request $request)
  {
    $secret = Secret::findOrFail($id);
    $message = '';
    if (Hash::check($request->password, $secret->password)) {
      $message = $this->personalDecrypt($secret->message, $request->password);
    } else {
      $message = 'Incorrect passphrase';
    }
    
    $secret->delete();
    return view('secret', [ 'message' => $message ]);
  }
  
  public function accessform($id)
  {
    $secret = Secret::find($id);
    $message = '';
    if ($secret == null) {
      $message = 'Secret does not exist';
      return view('secret', [ 'message' => $message ]);
    }
    return view('accessform', [ 'id' => $id ]);
  }

  /**
   * Crypt that cannot be disclosed by developer who has access to DB but doesn't know original password
   */
  public function personalEncrypt($msg, $password)
  {
    // This function does use Sodium crypt library
    // in order to use encryption functions, we have to provide $nonce(24 bytes) and $key(32 bytes)
    // we generate it from $password by simple string multiplying 
    $key = str_repeat($password, intdiv(32, strlen($password))) . substr($password, 0, 32%strlen($password));
    $nonce = str_repeat($password, intdiv(24, strlen($password))) . substr($password, 0, 24%strlen($password));
    $ciphertext = sodium_crypto_secretbox($msg, $nonce, $key);
    return utf8_encode($ciphertext);
  }

  public function personalDecrypt($ciphertext, $password)
  {
    $key = str_repeat($password, intdiv(32, strlen($password))) . substr($password, 0, 32%strlen($password));
    $nonce = str_repeat($password, intdiv(24, strlen($password))) . substr($password, 0, 24%strlen($password));
    $plaintext = sodium_crypto_secretbox_open(utf8_decode($ciphertext), $nonce, $key);
    return $plaintext;
  }
}
