# PHP Junior

### Goal:
Develop an application that allows to share secretes between users.

### Scenario:
- Anonymous user opens a page with a form that has 2 fields for secret text and passphrase.
- Anonymous user submits the form and retrieves a secret link that is supposed to be shared with another user.
- Person who gets this link can open it and type a passphrase in the form. If the passphrase is correct secret is displayed.
- Only one attempt is allowed to enter passphrase (doesn't matter if the passphrase was correct or not). After that secret cannot be accessed anymore.

![](images/actors.png)
![](images/form1.png)
![](images/form2.png)

### Requirements:
- Use Symfony 4 or 5, PHP 7.4+, MySQL
- Fill **TODO** section with instructions on how to run and use the application.
- Keep secrets encrypted, use a strong encryption algorithm. Do not allow developers to read your secret.

### Nice to have:
- Tests

### How to work on the task:
1. Create fork of this repository.
2. Attentively read all requirements.
3. Use any IDE.
4. Use any approach to setup your application infrastructure.
5. Push the solution to forked repository.
6. Create Merge request and send it for review.

### Definition of done:
1. You can successfully go through a described scenario.
2. There is no any 500 errors.
3. TODO section is completed and reviewer can run the application on his computer following described steps.
4. If you have tests all of them are running succesfully.

### TODO:
