@extends('layout')

@section('body')
  @if($message)
    <p>{{ $message }}</p>
    <p><a href="/" style="border: solid 1px rgb(164, 156, 145); border-radius: 3px; padding: 8px; text-decoration: none; color: white; font-weight:700; ">Home</a></p>
  @endif
@endsection