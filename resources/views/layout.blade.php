<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@isset($title) {{ $title }} | @endisset {{ config('app.name', 'Laravel') }}</title>

    </head>
    <body style="background-color: rgb(29, 31, 32); color: rgb(164, 156, 145); ">
      @yield('body')
    </body>
</html>