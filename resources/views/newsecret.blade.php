<!DOCTYPE html>
<html lang="en">
<head>
  <title>New Secret</title>
</head>
<body style="background-color: rgb(29, 31, 32); color: rgb(164, 156, 145); ">

@if(session('status'))
  <div class="success" style="border-radius: 7px; background: #73AD21; padding: 20px; margin: 10px 0; color: white;">
    {{ session('status') }}
    @if(session('newsecreturl'))
      <a href="{{ session('newsecreturl') }}">{{ session('newsecreturl') }}</a>
    @endif
  </div>
@endif

@if ($errors->any())
    <div style="border-radius: 7px; background: salmon; padding: 20px; margin: 10px 0; color: white;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <form action="/" method="post" style="border: solid 1px rgb(164, 156, 145); padding: 5px; max-width: 300px">
    @csrf
    <h3>New secret</h3>
    <p><div>Message: </div><textarea name="message" rows="5">{{ old('message') }}</textarea></p>
    <p><div>Passphrase: </div><input type="password" name="password"></p>
    <input type="submit">
  </form>

</body>
</html>