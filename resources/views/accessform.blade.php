@extends('layout')

@section('body')
<form action="/secret/{{ $id }}" method="post" style="border: solid 1px rgb(164, 156, 145); padding: 5px; max-width: 300px">
    @csrf
    <h3>Secret retrieving</h3>
    <p>To gain access to the secret, input passphrase</p>
    <p><span>Passphrase: </span><input type="password" name="password"></p>
    <input type="submit">
  </form>
@endsection