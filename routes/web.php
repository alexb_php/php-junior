<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\SecretController;
use App\Secret;
use Illuminate\Http\Request;

Route::get('/', function () {
  return view('newsecret');
});

Route::post('/', 'SecretController@store');

Route::get('/secret/{id}', 'SecretController@accessform');

Route::post('/secret/{id}', 'SecretController@retrieveById');

Route::get('/secret/{id}/delete', 'SecretController@deleteById');